# xSTART

This repository contains the files necessary to recreate the xSTART xApp Development Environment. Moreover, this README serves as documentation for the environment!

## Environment structure

![](SimEnvFigure.png)

### Containers

- `db`: Part of the RIC. Redis database, used by other RIC elements as a shared data layer (more accurately, a message-oriented middleware). Uses the ColO-RAN RIC through Docker image `emoro/dbaas`.
- `e2rtmansim`: Part of the RIC. E2 Message Router, for internal messages within the RIC. Also adjusts the differences between simulation time and real time in terms of E2 messages. Uses the ColO-RAN RIC through Docker image `emoro/e2rtmansim`.
- `e2mgr`: Part of the RIC. Manages the E2 termination and related messaging, as well as connection to other components and the shared data layer. Uses the ColO-RAN RIC through Docker image `emoro/e2mgr`.
- `e2term`: Part of the RIC. E2 termination implementation. Uses the ColO-RAN RIC through Docker image `emoro/e2term`.
- `xapp`: xApp container. Runs an example xApp, which connects to the RIC and executes certain logic. The current version executes the ColO-RAN example xApp (folder `coloran-xapp`).
- `ns-o-ran`: ns-3 simulator, including the O-RAN extensions (namely, O-RAN interface support, and a modified version of e2sim to support E2 termination(s) on gNBs). Uses a customized version of the original ns-O-RAN from North-Western University, stored in folder `ns-o-ran-kai`.
- All containers are able to communicate with each other through network `ric` (`10.0.2.0/24`). Nonetheless, **this is the only network they communicate with**. Containers are not connected to host/bridge networks on your device, do not expect to reach them via port redirection.

## Using the environment

1. Ensure you have Docker and Docker-Compose available in your machine.
2. Clone this repository and open the directory.
3. `docker-compose up --build` (to guarantee all images are re-built if necessary).
4. Ensure the E2 terminations are properly communicating.
5. Once you are done working, run `docker-compose down` to remove the containers and clean up your environment. Restarting the simulation involves `docker-compose down && docker-compose up --build`.